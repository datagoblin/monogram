set dotenv-load

tag_regular := "[${c_TAN}REGULAR${c_END}]"
tag_italics := "[${c_TAN}ITALICS${c_END}]"

# dang wouldn't it be great if the PXF -> TTF and whole BMF steps were command-lineable ?

# INPUT PATHS
dir_source := "source"
abs_source := absolute_path(dir_source)
uri_source := "file:///" + replace(abs_source,"\\", "/") + "/"

export FONT_BASE   := dir_source / "pxf" / "monogram-extended-base.pxf"
export FONT_ITALIC := dir_source / "pxf" / "monogram-extended-italic.pxf"


# OUTPUT PATHS
dir_merge     := "out/1.merge"
dir_build_pxf := "out/2.build_pxf"
dir_build_bmf := "out/3.build_bmf"
dir_meta      := "out/4.meta"
dir_pack      := "out/5.pack"

abs_merge     := absolute_path(dir_merge)
abs_build_pxf := absolute_path(dir_build_pxf)
abs_build_bmf := absolute_path(dir_build_bmf)
abs_meta      := absolute_path(dir_meta)
abs_pack      := absolute_path(dir_pack)

uri_merge     := "file:///" + replace(abs_merge,     "\\", "/") + "/"
uri_build_pxf := "file:///" + replace(abs_build_pxf, "\\", "/") + "/"
uri_build_bmf := "file:///" + replace(abs_build_bmf, "\\", "/") + "/"
uri_meta      := "file:///" + replace(abs_meta,      "\\", "/") + "/"
uri_pack      := "file:///" + replace(abs_pack,      "\\", "/") + "/"


# RECIPES

# show instructions and command list
help:
  #!sh
  echo -e "[${c_YLW}MONOGRAM${c_END} v${FONT_VERSION}]"
  echo -e "\n${c_TAN}SOURCE${c_END}"
  echo -e "${c_YLW}{{uri_source}}${c_END}"
  shtree -CFAa --dirsfirst {{dir_source}} | tail -n +2 | head -n -2 
  echo -e "\n${c_TAN}STEPS"
  just _pstep 1 "${c_YLW}just ${c_BLU}merge${c_END}: merge and prepare .PXF files for exporting"
  just _pstep 2 "${c_YLW}just ${c_BLU}build${c_END}: build .TTF files (${c_RED}done manually!${c_END})"
  just _pstep 3 "${c_YLW}just ${c_BLU}meta${c_END}:  inject metadata into .TTF files"
  just _pstep 4 "${c_YLW}just ${c_BLU}pack${c_END}:  package files for deployment"
  just _pstep 5 "${c_YLW}just ${c_BLU}push${c_END}:  push packaged files to itch.io"
  echo -e "\n${c_TAN}TIP${c_END}: run ${c_YLW}just ${c_BLU}ls${c_END} for a full list of commands.\n"

# list available commands
ls:
  #!sh
  just --list --unsorted --list-heading '' --list-prefix '  '

# retrieve build status from itch.io
status:
  #!sh
  butler status ${ITCH_PATH} | bat --theme="ansi" --paging="never" --pager="" -p -l swift


# generate merged .PXF files and inject export data
merge: && _merge-regular _merge-italic

_merge-regular:
  #!sh
  FONT_OUT="{{dir_merge}}/monogram-extended.pxf"
  FONT_EXPORT='{{abs_build_pxf}}\monogram-extended.ttf'
  FONT_EXPORT=$(sed 's|\\|\\\\|g' <<<"$FONT_EXPORT") # escape backslashes
  cp ${FONT_BASE} ${FONT_OUT}
  sed -i "s|\(last_exported_path: \).\+|\1${FONT_EXPORT}|" ${FONT_OUT}
  echo -e "{{tag_regular}} nothing to merge, copying font to: ${c_YLW}${FONT_OUT}${c_END}"

_merge-italic:
  #!sh
  export FONT_OUT="{{dir_merge}}/monogram-extended-italic-merged.pxf"
  export FONT_DIFF=${FONT_ITALIC}
  export FONT_EXPORT='{{abs_build_pxf}}\monogram-extended-italic.ttf'
  python scripts/merge.py
  echo -e "{{tag_italics}} merged font project file saved to: ${c_YLW}${FONT_OUT}${c_END}"

# generate output .TTF files
build:
  #!sh
  just _pstep 1 "open directory:" "${c_YLW}{{uri_merge}}${c_END}"
  just _pstep 2 "use PixelForge to export .PXF files to .TTF at:" "${c_YLW}{{uri_build_pxf}}${c_END}"
  just _pstep 3 "upload .TTF files to:" "${c_BLU}https://ptoys.sakura.ne.jp/bitfontimport/${c_END}" \
                "  {{tag_regular}} Font-Size: 352px" \
                "  {{tag_italics}} Font-Size: 384px" \
                "  [${c_GRN}x${c_END}] Monospace ; Width: 6px"
  just _pstep 4 "re-generate new .TTF files, and save them to:" "${c_YLW}{{uri_build_bmf}}${c_END}"


# inject metadata into .TTF files
meta:
  #!sh
  just _meta "Regular" "monogram-regular" "monogram-extended"        "{{tag_regular}}"
  just _meta "Italic"  "monogram-italic"  "monogram-extended-italic" "{{tag_italics}}"

_meta style filename_in filename_out tag:
  #!sh
  export FONT_STYLE="{{style}}"
  export FONT_PATH="{{dir_build_bmf}}/{{filename_in}}.ttf"
  export FONT_OUT="{{dir_meta}}/{{filename_out}}.ttf"
  # suppress glyph mapping errors
  ffpython scripts/set_meta.ff.py 2> >(grep -v 'mapped to')
  echo -e "{{tag}} metadata-enriched file saved to: ${c_YLW}${FONT_OUT}${c_END}"


# package output files into an archive for deployment
pack: && _pack-zip _pack-channels

_pack-zip:
  #!sh
  rm -rf "./{{dir_pack}}/"*
  PACK_OUT="{{dir_pack}}/monogram"
  mkdir ${PACK_OUT}
  cp -pr "{{dir_meta}}" "${PACK_OUT}/ttf"
  cp -pa "{{dir_source}}/ttf/." "${PACK_OUT}/ttf"
  cp -pr "{{dir_source}}/bitmap" "${PACK_OUT}/bitmap"
  cp -pr "{{dir_source}}/pico-8" "${PACK_OUT}/pico-8"
  cp -p  "{{dir_source}}/credits/credits.txt" "${PACK_OUT}/credits.txt"
  7z -bsp0  -bso0 a -tzip "${PACK_OUT}.zip" "./${PACK_OUT}"
  echo -e "font files packed into archive: ${c_YLW}${PACK_OUT}.zip${c_END}"

_pack-channels:
  #!sh
  PACK_ROOT="{{dir_pack}}/monogram"
  CHANNEL_ROOT="{{dir_pack}}/channels"
  mkdir -p $( eval echo "{{dir_pack}}/channels/{${BUTLER_CHANNELS}}" )
  set_channel_payload() { cp -pa "${PACK_ROOT}$2" "${CHANNEL_ROOT}/$1" ; }
  set_channel_payload "zip"       ".zip"
  set_channel_payload "ttf-light" "/ttf/monogram.ttf"
  set_channel_payload "ttf-x-reg" "/ttf/monogram-extended.ttf"
  set_channel_payload "ttf-x-ita" "/ttf/monogram-extended-italic.ttf"
  set_channel_payload "p8"        "/pico-8/monogram.p8"
  set_channel_payload "bmp-png"   "/bitmap/monogram-bitmap.png"
  set_channel_payload "bmp-json"  "/bitmap/monogram-bitmap.json"
  echo -e "channels prepared for deployment:"
  echo -e "  ${c_BLU}${BUTLER_CHANNELS}${c_END}" | sed 's|,|\n  |g'

# push packaged files to itch.io with butler
push:
  #!sh
  echo -e "pushing ${c_YLW}v${FONT_VERSION}${c_END} to ${c_BLU}${ITCH_PATH}${c_END}...\n"
  for channel in ${BUTLER_CHANNELS//,/ } ; do
    PAYLOAD="{{dir_pack}}/channels/${channel}"
    butler push ${PAYLOAD} "${ITCH_PATH}:${channel}" --userversion=${FONT_VERSION}
  done

#
rec out="demo.cast":
  powersession rec -f {{out}}

_pstep num msg1 msg2='' msg3='' msg4='' msg5='':
  #!sh
  echo -e "  ${c_TAN}{{num}}${c_END}) {{msg1}}"
  if [ -n "{{msg2}}" ];
    then echo -e "     {{msg2}}";
  fi
  if [ -n "{{msg3}}" ];
    then echo -e "     {{msg3}}";
  fi
  if [ -n "{{msg4}}" ];
    then echo -e "     {{msg4}}";
  fi
  if [ -n "{{msg5}}" ];
    then echo -e "     {{msg5}}";
  fi

