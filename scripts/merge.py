import os
import yaml

font_base = os.environ.get("FONT_BASE")
font_diff = os.environ.get("FONT_DIFF")
font_out = os.environ.get("FONT_OUT")

font_export = os.environ.get("FONT_EXPORT")

def read_pxf(filename):
    with open(filename) as f:
        raw_data = f.read().replace("\t", "  ")
        return yaml.safe_load(raw_data)

font_data = read_pxf(font_base)
diff_data = read_pxf(font_diff)

for key in ("font_family_name","font_sub_family_name", "version"):
    font_data[key] = diff_data[key]

font_data["glyphs"].update(diff_data["glyphs"])
font_data["num_glyphs"] = len(font_data["glyphs"])
font_data["last_exported_path"] = font_export

with open(font_out, "w+") as f:
    yaml_data = yaml.dump(font_data, width=2147483647, sort_keys=False)
    yaml_data = yaml_data.replace("  ", "\t")
    yaml_data = yaml_data.replace(": null", ": ")
    yaml_data = f"# PixelForge Font File\n{yaml_data}"
    f.write(yaml_data)

#print(f"merged font saved to: {font_out}")