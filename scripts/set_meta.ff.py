import contextlib
import fontforge
import os
import sys

from datetime import datetime

filepath = os.environ.get("FONT_PATH")
filepath_out = os.environ.get("FONT_OUT")

font_family = os.environ.get("FONT_FAMILY")
font_style = os.environ.get("FONT_STYLE")

font_version = os.environ.get("FONT_VERSION")
font_creator = os.environ.get("FONT_CREATOR")
font_website = os.environ.get("FONT_WEBSITE")
font_details = os.environ.get("FONT_DETAILS")
font_pangram = os.environ.get("FONT_PANGRAM")

year = datetime.now().year

metadata = {
	"Copyright":		f"Copyright {font_creator} {year}",
	"Family":			f"{font_family}",
	"SubFamily":		f"{font_style}",
	"UniqueID":			f"{font_family}",
	"Fullname":			f"{font_family} {font_style}",
	"Version":			f"Version {font_version}",
	"PostScriptName":	f"{font_family}",
	"Manufacturer":		f"{font_creator}",
	"Designer":			f"{font_creator}",
	"Descriptor":		f"{font_details}",
	"Vendor URL":		f"{font_website}",
	"Designer URL":		f"{font_website}",
	"License":			f"Creative Commons Zero v1.0 Universal",
	"License URL":		f"https://creativecommons.org/publicdomain/zero/1.0/",
	"Sample Text":		f"{font_pangram}",
}

bmf_font = fontforge.open(f"{filepath}", 16)

def set_metadata(font, key, value):
	font.appendSFNTName('English (US)', key, value)

for k,v in metadata.items():
	set_metadata(bmf_font, k, v)

bmf_font.generate(f"{filepath_out}")

bmf_font.close()